package com.nimaaa5.ecommerceexample.validator;

import com.nimaaa5.ecommerceexample.dto.UserDto;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class FieldMatchValidator implements ConstraintValidator<FieldMatch, Object> {

    private String firstFieldName;
    private String secondFieldName;

    @Override
    public void initialize(final FieldMatch constraintAnnotation) {

    }

    @Override
    public boolean isValid(final Object value, final ConstraintValidatorContext context) {
//        try {
//            final Object firstObj = BeanUtils.getProperty(value, firstFieldName);
//            final Object secondObj = BeanUtils.getProperty(value, secondFieldName);
//            return firstObj == null && secondObj == null || firstObj != null && firstObj.equals(secondObj);
//        } catch (final Exception ignore) {
//            // ignore
//        }
        if (value instanceof UserDto) {
            UserDto user = (UserDto) value;
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(context.getDefaultConstraintMessageTemplate())
                    .addPropertyNode( "passwordConfirm" ).addConstraintViolation();
            return user.getPassword().equals(user.getPasswordConfirm());
        }
        return false;
    }
}
