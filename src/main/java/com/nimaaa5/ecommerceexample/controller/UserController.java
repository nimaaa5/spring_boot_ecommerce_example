package com.nimaaa5.ecommerceexample.controller;

import com.nimaaa5.ecommerceexample.dto.UserDto;
import com.nimaaa5.ecommerceexample.entity.User;
import com.nimaaa5.ecommerceexample.service.UserServiceDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping(name = "/user")
public class UserController {

    private UserServiceDetailsImpl userService;


    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserController(UserServiceDetailsImpl userService, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userService = userService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        ;
    }

    @GetMapping("/login")
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Your username and password is invalid.");

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");
        return "login";
    }

    @GetMapping("/sign-up")
    public String showSignUp(Model model) {
        model.addAttribute("userForm", new UserDto());
        return "sign-up";
    }


    @PostMapping(value = "/sign-up")
    public String processForm(@Valid @ModelAttribute("userForm") UserDto form,
                              BindingResult theBindingResult, ModelMap model) {
        if (theBindingResult.hasErrors()) {
            return "sign-up";
        } else {
            boolean isEmailValid = userService.isUserExistByEmail(form.getEmail());
            boolean isUsernameValid = userService.isUserExistByUsername(form.getUsername());
            if (!isEmailValid && !isUsernameValid) {
                form.setPassword(bCryptPasswordEncoder.encode(form.getPassword()));
                userService.saveUser(form);
                return "redirect:/";
            } else {
                if (isEmailValid){
                model.addAttribute("errorMessage", "Email is not valid");
                }
                if (isUsernameValid){
                    model.addAttribute("errorMessage", "Username is not valid");
                }
                return "sign-up";
            }
        }
    }


}
