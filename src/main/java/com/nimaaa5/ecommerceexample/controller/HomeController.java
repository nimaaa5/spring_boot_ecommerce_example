package com.nimaaa5.ecommerceexample.controller;

import com.nimaaa5.ecommerceexample.entity.Product;
import com.nimaaa5.ecommerceexample.entity.ProductCategory;
import com.nimaaa5.ecommerceexample.service.ProductCategoryService;
import com.nimaaa5.ecommerceexample.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class HomeController {

    private ProductService productService;
    private ProductCategoryService productCategoryService;

    @Autowired
    public HomeController(ProductService productService, ProductCategoryService productCategoryService) {
        this.productService = productService;
        this.productCategoryService = productCategoryService;
    }


    @GetMapping(value = "/")
    public String getAllByCategory(@RequestParam(value = "category", required = false) String category,
                                   @RequestParam(value = "size", required = false) Integer size,
                                   @RequestParam(value = "page", required = false) Integer page,
                                   Model model) {
        model.addAttribute("categories", productCategoryService.findAllOrderByName());


        if (category != null && !isBlank(category)) {
            ProductCategory productCategory = productCategoryService.findByName(category);
            if (productCategory == null) {
                throw new IllegalArgumentException("Invalid category parameter");
            }
            List<Product> products = productService.findAllByProductCategory(productCategory, page ,size);
            model.addAttribute("products", products);
        } else {
            model.addAttribute("products", productService.findAll(page, size));
        }


        return "home";
    }

    @GetMapping(value = "/product")
    public String searchProduct(@RequestParam(value = "search", required = false) String searchedProductText,
                                @RequestParam(value = "page", required = false) Integer page,
                                @RequestParam(value = "size", required = false) Integer size,
                                Model model) {

        model.addAttribute("categories", productCategoryService.findAllOrderByName());

        if (searchedProductText != null && !isBlank(searchedProductText)) {
            List<Product> products = productService.findAllProductBySearchedText(searchedProductText, page, size);
            if (products.size() == 0) {
                throw new IllegalArgumentException("Invalid category parameter");
            }
            model.addAttribute("products", products);
        }
        return "home";
    }


    private boolean isBlank(String param) {
        return param.isEmpty() || param.trim().equals("");
    }

    private Sort getSort(String sort) {
        switch (sort) {
            case "lowest":
                return Sort.by(Sort.Direction.ASC, "price");
            case "highest":
                return Sort.by(Sort.Direction.DESC, "price");
            case "name":
                return Sort.by(Sort.Direction.ASC, "name");
            default:
                return null;
        }
    }
}
