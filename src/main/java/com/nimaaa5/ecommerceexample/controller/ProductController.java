package com.nimaaa5.ecommerceexample.controller;

import com.nimaaa5.ecommerceexample.entity.Product;
import com.nimaaa5.ecommerceexample.service.ProductCategoryService;
import com.nimaaa5.ecommerceexample.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class ProductController {

    private ProductService productService;
    private ProductCategoryService productCategoryService;

    @Autowired
    public ProductController(ProductService productService, ProductCategoryService productCategoryService) {
        this.productService = productService;
        this.productCategoryService = productCategoryService;
    }

    @GetMapping(value = "/products/{productId}")
    public String getById(@PathVariable("productId") long productId, Model model){
        Product product = this.productService.findById(productId);
        model.addAttribute("products",product);
        model.addAttribute("categories",productCategoryService.findAllOrderByName());
        return "product-details";
    }


}
