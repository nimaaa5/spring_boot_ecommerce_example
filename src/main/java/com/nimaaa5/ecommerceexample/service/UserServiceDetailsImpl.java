package com.nimaaa5.ecommerceexample.service;

import com.nimaaa5.ecommerceexample.dao.UserRepository;
import com.nimaaa5.ecommerceexample.dto.UserDto;
import com.nimaaa5.ecommerceexample.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceDetailsImpl implements UserDetailsService, UserService {

    private UserRepository userRepository;



    @Autowired
    public UserServiceDetailsImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findByUserName(userName);
        user.orElseThrow(() -> new UsernameNotFoundException("Not found " + userName));
        return user.map(com.nimaaa5.ecommerceexample.entity.UserDetails::new).get();
    }




    public void saveUser(UserDto user) {
        User newUser = new User();
        newUser.getAuthorities().add(String.valueOf(new SimpleGrantedAuthority("ROLE_CUSTOMER")));
        newUser.setEnabled(1);
        newUser.setId(0);
        newUser.setEmail(user.getEmail());
        newUser.setUserName(user.getUsername());
        newUser.setPassword(user.getPassword());
        this.userRepository.save(newUser);
    }


    @Override
    public boolean isUserExistByEmail(String email) {
        return userRepository.existsByEmail(email);
    }


    @Override
    public boolean isUserExistByUsername(String username) {
        return userRepository.existsByUserName(username);
    }
}
