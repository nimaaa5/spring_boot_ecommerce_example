package com.nimaaa5.ecommerceexample.service;

import com.nimaaa5.ecommerceexample.dto.UserDto;
import com.nimaaa5.ecommerceexample.entity.User;
import org.springframework.stereotype.Service;


public interface UserService {
    void saveUser(UserDto user);

    boolean isUserExistByEmail(String email);

    boolean isUserExistByUsername(String username);
}
