package com.nimaaa5.ecommerceexample.dao;

import com.nimaaa5.ecommerceexample.entity.ProductCategory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "productCategory", path = "product-category")
public interface ProductCategoryRepository extends CrudRepository<ProductCategory, Long> {

    ProductCategory findAllByCategoryName(String category);
    List<ProductCategory> findAllByOrderByCategoryName();

}
