package com.nimaaa5.ecommerceexample.dao;

import com.nimaaa5.ecommerceexample.entity.User;
import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Integer> {


    Optional<User> findByUserName(String username);


    boolean existsByEmail(String email);


    boolean existsByUserName(String userName);


     User save(User s);
}
