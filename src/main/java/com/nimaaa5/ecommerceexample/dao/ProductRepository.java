package com.nimaaa5.ecommerceexample.dao;


import com.nimaaa5.ecommerceexample.entity.Product;
import com.nimaaa5.ecommerceexample.entity.ProductCategory;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;


import java.util.List;
import java.util.Optional;


public interface ProductRepository extends CrudRepository<Product, Long> {
    List<Product> findAllByProductCategory(ProductCategory productCategory, Pageable pageable);
    List<Product> findAll(Pageable pageable);
    Optional<Product> findById(Long id);
    List<Product> findByNameContainingIgnoreCase(String searchedText, Pageable pageable);
}
